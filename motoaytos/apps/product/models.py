import uuid

from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from djmoney.models.fields import MoneyField
from taggit.managers import TaggableManager
from taggit.models import GenericUUIDTaggedItemBase, TaggedItemBase
from django_extensions.db.models import TimeStampedModel, \
    TitleSlugDescriptionModel, \
    ActivatorModel

from motoaytos.apps.utils.const import AVAILABILITY_CHOICES, ACTIVE_CURRENCY


class UUIDTaggedItem(GenericUUIDTaggedItemBase, TaggedItemBase):
    # If you only inherit GenericUUIDTaggedItemBase, you need to define
    # a tag field. e.g.
    # tag = models.ForeignKey(Tag, related_name="uuid_tagged_items", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class Product(TimeStampedModel,
              TitleSlugDescriptionModel,
              ActivatorModel):
    """
    Following the fields from here:
    https://www.motorcyclespareparts.eu/en
	https://www.motorparts-online.com/en
    """

    title = models.CharField(_('Title'),
                             max_length=255,
                             blank=True,
                             default=None)

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    categories = models.ManyToManyField('category.Category',
                                        blank=True,
                                        default=None,
                                        related_name='product')

    sub_categories = models.ManyToManyField('category.SubCategory',
                                            blank=True,
                                            default=None,
                                            related_name='product')

    brand = models.ForeignKey(_('Brand'),
                              max_length=255,
                              on_delete=models.CASCADE)
    image = models.ForeignKey(_('Image'),
                              on_delete=models.CASCADE)

    # default= 2 means that the product is available check utils/const.py
    availability = models.IntegerField(_('Availability'), choices=AVAILABILITY_CHOICES, default=2)
    quantity = models.IntegerField(_('Quantity'))

    # https://resources.datafeedwatch.com/academy/mpn
    # Manufacturer Part Number Identificatior
    mpn = models.CharField(max_length=64,
                           default=None,
                           blank=True)

    # https://www.thebalancesmb.com/what-is-a-sku-in-retail-terms-2890158
    # Stock Keeping Unit
    sku = models.CharField(max_length=64,
                           default=None,
                           blank=True)

    # Physical Attributes
    weight = models.DecimalField(_('Weight'),
                                 max_digits=8,
                                 decimal_places=4,
                                 default=None,
                                 blank=True,
                                 null=True)

    length = models.DecimalField(_('Length'),
                                 max_digits=8,
                                 decimal_places=4,
                                 default=None,
                                 blank=True,
                                 null=True)

    width = models.DecimalField(_('Width'),
                                max_digits=8,
                                decimal_places=4,
                                default=None,
                                blank=True,
                                null=True)

    Height = models.DecimalField(_('Height'),
                                 max_digits=8,
                                 decimal_places=4,
                                 default=None,
                                 blank=True,
                                 null=True)

    # Currency
    price = MoneyField(_('Price'), max_digits=14,
                       decimal_places=2,
                       default_currency=ACTIVE_CURRENCY)

    # Specifics

    is_featured = models.NullBooleanField(_('Featured'))

    tags = TaggableManager(through=UUIDTaggedItem)

    def __repr__(self):
        return str(self)

    def get_absolute_url(self):
        return reverse('product:detail', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class Image(models.Model):
    image = models.ImageField(upload_to='products/%Y/%m/%d/')

    def __str__(self):
        return str(self.image)

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')


class Brand(models.Model):
    name = models.CharField(_('Name'), max_length=255)

    def __repr__(self):
        return str(self)

    def get_absolute_url(self):
        return reverse('product:detail', kwargs={'name': self.name})

    class Meta:
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')
