from django.contrib import admin
from .models import Product, Image, Brand
# Register your models here.

Models = (Product, Image, Brand)

admin.site.register(Models)