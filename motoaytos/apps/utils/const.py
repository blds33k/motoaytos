from django.utils.translation import gettext_lazy as _

AVAILABILITY_CHOICES = (
    (1, _('Not Available')),
    (2, _('Available')),
    (2, _('Upon Request'))
)

ACTIVE_CURRENCY = 'BGN'
