from django.db import models
from django.utils.translation import gettext_lazy as _

from django_extensions.db.models import TimeStampedModel, \
    TitleSlugDescriptionModel, \
    ActivatorModel


# Create your models here.
class Category(TimeStampedModel,
               TitleSlugDescriptionModel,
               ActivatorModel):

    name = models.CharField(_('Name'), max_length=255)
    sub_category = models.ManyToManyField('SubCategory',
                                          blank=True,
                                          default=None,
                                          related_name='category')

    def __repr__(self):
        return str(self)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class SubCategory(TimeStampedModel,
                  TitleSlugDescriptionModel,
                  ActivatorModel):

    def __repr__(self):
        return str(self)

    class Meta:
        verbose_name = _('Subcategory')
        verbose_name_plural = _('Subcategories')
