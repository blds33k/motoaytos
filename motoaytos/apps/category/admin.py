from django.contrib import admin
from .models import Category, SubCategory


Models = (Category, SubCategory)


admin.site.register(Models)
